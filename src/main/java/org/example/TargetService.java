package org.example;

public class TargetService {
    public String doSomething(){
        System.out.println("Something");
        return "Something";
    }

    public String doSomethingElse(){
        System.out.println("Something Else");
        return "Something Else";
    }
}
