package org.example.integration;

import org.example.TargetService;
import org.junit.jupiter.api.Test;

public class TargetService_IT {
    @Test
    public void somethingItTest() throws Exception{
        System.out.println("Running Integration Test");
        Thread.sleep(1000);
        TargetService targetService = new TargetService();
        targetService.doSomethingElse();
    }
}
