# Purpose
This project is a bare-bones example of the use of the maven failsafe plugin. 
It is nothing more than a default maven archetype loaded with one additional dependency (failsafe), 
and one additional class + tests.

Look towards the configuration of the POM to see that the failsafe plugin 
is configured to execute on the verify goal, but no others.

This basically means that in a typical build cycle, integration tests will not 
be ran (`mvn package`). You still retain the ability to execute them as desired however, 
with a simple flag or additional call in the CLI.  
The current setup has integration tests being ran only on `mvn verify`, which is isolated 
from many of the default goals in the package lifecycle. 

Integration tests are registered with the plugin in almost the same 
way that traditional unit tests are with the failsafe plugin. 
Anything in the standard test folder that maven watches, which starts or ends 
with 'IT', will be considered an integration test. 

All aspects of this process can be customized, including:
- when the integration test runs (verify/package/etc.)
- naming convention for integration test files (instead of `*IT`)
- scan directories (instead of the traditional test folder)
- excluded tests/directories (specialty tests that should never be detected for some reason)


By relying on this plugin instead of isolating the integration tests 
in non-standard locations or in separate projects, it is 
easier for the average developer to intuit function, as well as 
allow more options in the future towards test execution and customization.

You also will not ever have to argue with your IDE about what 
is and is not a proper project file, which I view as a huge benefit.